#include "expressionbase.h"
#include <stdlib.h>

ExpressionBase *new_expression_base() {
    ExpressionBase *base = malloc(sizeof(ExpressionBase));
    return base;
}

void add_tail(ExpressionBase *base, Expression *expr) {
    expr->next = 0;
    expr->prev = 0;

    if (base->last) {
        base->last->next = expr;
        expr->prev = base->last;
    }
    if (base->first == 0) base->first = expr;
    base->last = expr;
    return;
}
