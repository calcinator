#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "expression.h"
#include "expressionbase.h"

#define MAX 4096

int main(int argc, char* argv[]) {
    int i = 0, factor = 1;
    unsigned char next;
    char expression[MAX];
    ExpressionBase *base = new_expression_base();
    Expression *expr = malloc(sizeof(Expression)), *prevp;


    while(next = getchar()) {
        if(next == '\n') { // We hit end of line, make last expression and break
            if (get_value(expression)) { // We don't have an empty expression
                finish_expression(expr, expression, next, factor);
                add_tail(base, expr);
            }
            break;
        }

        if(isdigit(next) || next == '.') { // We hit a number - add that to the stack
            if (i == 0 && base->last && base->last->op == 0) { // after (
                base->last->op = '*';
            }
            expression[i] = next;
            ++i;
        } else if ( next == '*' || next == '/' || next == '+' || next == '-' || next == '^') { // We hit an operator, make a new expression
            if (i == 0) { // after (
                base->last->op = next;
                continue;
            }
            finish_expression(expr, expression, next, factor);
            add_tail(base, expr);

            expr = malloc(sizeof(Expression));
            strncpy(expression, "\0", strlen(expression));
            i = 0;
        } else if (next == '(') {
            if (get_value(expression)) {
                finish_expression(expr, expression, '*', factor);
                add_tail(base, expr);
            }
            expr = malloc(sizeof(Expression));
            strncpy(expression, "\0", strlen(expression));
            i = 0;
            factor *= 10;
        } else if (next == ')') {
            finish_expression(expr, expression, 0, factor);
            add_tail(base, expr);

            expr = malloc(sizeof(Expression));
            strncpy(expression, "\0", strlen(expression));
            i=0;
            factor /= 10;
        } else { // We didn't hit a digit or an operator - do nothing
            continue;
        }
    }

    for (expr = base->first; expr; expr = expr->next) {
        evaluate(expr);
        prevp = expr;
    }
    printf("= %.1f\n", prevp->value);
    return 0;
}

