#include "expression.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int get_precedence(unsigned char op) {
    switch(op) {
        case('^'):
            return 3;
            break;
        case('*'):
        case('/'):
            return 2;
            break;
        case('+'):
        case('-'):
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

double apply_operator(unsigned char op, double value1, double value2) {
#if DEBUG
    printf("Applying %c on %f and %f\n", op, value1, value2);
#endif
    switch(op) {
        case('^'):
            return pow(value1, value2);
            break;
        case('*'):
            return value1*value2;
        case('/'):
            return value1/value2;
            break;
        case('+'):
            return value1+value2;
        case('-'):
            return value1-value2;
            break;
        default:
            return -1;
            break;
    }
}

double get_value(char *string) {
    int i = strlen(string);
    if (!strchr(string, '.')) {
        strcat(string, ".0\0");
    }
    return atof(string);
}

void evaluate(Expression *expr) {
    /**
     * XXX first expression?
     * If expr->precendence < next->precedence:
     *      evaluate(next);
     *  new_value = apply_operator(expr->op, expr->value, next->value);
     *  next->value = new_value;
     *  next->prev = expr->prev;
     *  prev->next = expr->next;
     */
    Expression *next = expr->next;
    double new_value;
    if (!next) { // Last expression, do nothing(must be an integer constant)
        return;
    }
    if (expr->precedence < next->precedence) { // Next expression has higher precendence
        evaluate(next);
        evaluate(expr); // Re-evaluate expr again
        return;
    }
    new_value = apply_operator(expr->op, expr->value, next->value);
    next->value = new_value;

    // Unlink expr
    if (expr->prev) { // not first expression
        next->prev = expr->prev;
        expr->prev->next = next;
    } else { // first expression
        next->prev = 0;
    }
}

void finish_expression(Expression *expr, char *expression, unsigned char op, int precedence_factor) {
    if (expr->prev && expr->prev->op == 0) {
        expr->prev->op = '*';
    }
    if (op == '\n') {
        expr->op = 0;
        expr->flags |= CONSTANT;
    } else {
        expr->op = op;
    }
    expr->value = get_value(expression);
    expr->precedence = get_precedence(op)*precedence_factor;
}


void print_expression(Expression *expr) {
    printf("Value: \t\t%f\nOperator: \t%c\nPrecedence: \t%d\n===\n", expr->value, expr->op, expr->precedence);
}
