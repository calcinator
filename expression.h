#ifndef _EXPR_H
#define _EXPR_H

#define DEBUG 1

enum {
    CONSTANT = 1,
    EVALUATED = 2
};

typedef struct expression {
    char flags;
    double value;
    unsigned char op;
    struct expression *prev;
    struct expression *next;
    int precedence;
} Expression;

int get_precedence(unsigned char op);
double apply_operator(unsigned char op, double value1, double value2);
double get_value(char *string);

void evaluate(Expression *expr);
void finish_expression(Expression *expr, char *expression, unsigned char op, int precedence_factor);
void print_expression(Expression *expr);
#endif
