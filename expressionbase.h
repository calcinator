#ifndef __EXPRESSION_BASE_H
#define __EXPRESSION_BASE_H

#include "expression.h"

typedef struct expressionBase {
    Expression *first, *last;
} ExpressionBase;

ExpressionBase *new_expression_base();
void add_tail(ExpressionBase *base, Expression *expr);

#endif
